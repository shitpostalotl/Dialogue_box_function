import time

def sp_print(message):
  for letter in message:
    print(letter, end="", flush = True)
    time.sleep(.1)
  print()

sp_print('THING!')
